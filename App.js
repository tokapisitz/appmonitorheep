import React, { Component } from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { View } from 'react-native';




import splash from './src/splash';
import login from './src/login';
import main from './src/main';


// const Main = createStackNavigator({
//   Main: { screen: main },

// });




const RootStack = createSwitchNavigator(
  {
    Splash: splash,
    Login: login,
    App: createStackNavigator({
      Main: main


    })


  },
  {
    initialRouteName: 'Splash',
  }
);

export default createAppContainer(RootStack);