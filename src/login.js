import React, { useState, useCallback, useMemo } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity } from 'react-native';
import { Button, Input } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import Loading from 'react-native-loading-spinner-overlay';
import axios from 'axios';
global.Buffer = global.Buffer || require('buffer').Buffer


const Login = () => {
    const { navigate } = useNavigation();

    const [isLoading, setLoading] = useState(false);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');



    onChangeTextUsername = (value) => {
        setUsername(value)
    }

    onChangeTextPassword = (value) => {
        setPassword(value)
    }


    showAlert = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {
                    text: 'OK',
                    onPress: () => {
                        setLoading(false)
                    }


                },
            ],
            { cancelable: false },
        );
    }


    callLogin = async (username, password) => {
        setLoading(true)

        const data = {
            "username": username,
            "password": password,
        }


        await axios.post('http://apicloud.bsisugarcane.com/api/CYMS/MonitorLogin', data)
            .then((response) => {
                console.log('data :  ', response.data)
                setLoading(false)
                if (response.data.message == 'Invalid Password') {
                    showAlert('ข้อความ', 'เข้าสู่ระบบไม่สำเร็จ กรุณาตรวจสอบ')
                } else {
                    AsyncStorage.setItem('accessToken', response.data.token);
                    navigate('Main')
                }
            })
            .catch((error) => {
                showAlert('ข้อความ', 'เข้าสู่ระบบไม่สำเร็จ กรุณาตรวจสอบ')
            })
            .finally(function () {
            });

    };




    return (
        <View style={{
            flex: 1,
            backgroundColor: "#fff",
            width: "100%",
        }}>
            {isLoading ?
                <Loading
                    visible={true}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                :
                <View style={styles.MainContainer}>
                    <View style={{
                        flex: 0.4, justifyContent: 'center',
                        alignItems: 'center',
                    }} >
                        <Image
                            style={styles.stretch}
                            source={require('../img/splash.jpg')}
                        ></Image>
                    </View>
                    <View style={{ flex: 0.6, }} >
                        <Input
                            overflow="hidden"
                            keyboardAppearance="dark"
                            placeholder="ผู้ใช้งาน"
                            inputContainerStyle={{ backgroundColor: "#FFF", borderColor: '#000', borderRadius: 8, borderWidth: 1, margin: 12, height: 50, paddingLeft: 12 }}
                            inputStyle={{ fontSize: 18 }}
                            onChangeText={(value) => {
                                onChangeTextUsername(value)
                            }}
                        />
                        <Input
                            overflow="hidden"
                            keyboardAppearance="dark"
                            placeholder="รหัสผ่าน"
                            secureTextEntry={true}
                            inputContainerStyle={{ backgroundColor: "#FFF", borderColor: '#000', borderRadius: 8, borderWidth: 1, margin: 12, height: 50, paddingLeft: 12 }}
                            inputStyle={{ fontSize: 18 }}
                            onChangeText={(value) => {
                                onChangeTextPassword(value)
                            }}
                        />
                        <Button
                            buttonStyle={{ backgroundColor: "#512D1A", height: 50, width: 160, alignSelf: "center", marginTop: 12, borderRadius: 4, borderWidth: 1, borderColor: '#000' }}
                            title="เข้าสู่ระบบ"
                            titleStyle={{ color: '#fff', fontSize: 16 }}
                            onPress={() => {
                                if (username == '') {
                                    showAlert('ข้อความ', 'กรุณากรอก username')
                                } else if (password == '') {
                                    showAlert('ข้อความ', 'กรุณากรอก password')
                                } else {
                                    callLogin(username, password)
                                }

                            }
                            }>
                        </Button>
                    </View>




                </View>
            }
        </View>
    );
}

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        width: 320,
        height: 120,
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },
    spinnerTextStyle: {
        color: '#FFF',
        fontSize: 20
    },

});

export default Login