import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, ImageBackground, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity } from 'react-native';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
const Splash = () => {

    const { navigate } = useNavigation();



    useEffect(() => {
        async function fetchMyAPI() {

            console.log('token : ', await AsyncStorage.getItem('accessToken'))
            if (await AsyncStorage.getItem('accessToken') == null) {
                setTimeout(() => {
                    navigate('Login')
                }, 5000)
            } else {

                navigate('Main')
            }

            // setTimeout(() => {
            //     navigate('Login')
            // }, 5000)

        }
        fetchMyAPI()
    }, [])

    // setTimeout(() => {
    //     navigate('Login')

    // }, 5000)

    return (
        <View style={styles.MainContainer}>

            <Image
                style={styles.stretch}
                source={require('../img/splash.jpg')}
            ></Image>

        </View>
    );
}

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    ImageContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    TokenContainer: {

        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        width: 320,
        height: 120,
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },

});

export default Splash