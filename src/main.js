import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity, ScrollView, TextInput, RefreshControl } from 'react-native';
import Ionicons from "react-native-vector-icons/FontAwesome5";
import { Divider, Input, Button, Card } from 'react-native-elements';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks';
import Loading from 'react-native-loading-spinner-overlay';
import axios from 'axios';
import 'intl'
import 'intl/locale-data/jsonp/en'


const Main = ({ navigation }) => {
    const { navigate } = useNavigation();
    const [isUpdateButtonPressed, setIsUpdateButtonPressed] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const [data, setData] = useState({});
    const [token, setToken] = useState('');



    useEffect(() => {
        navigation.setParams({
            setIsUpdateButtonPressed,
        });
    }, [])

    useEffect(() => {
        if (isUpdateButtonPressed) {
            setTimeout(() => {
                navigate('Login')
                AsyncStorage.setItem('accessToken', '');
                setIsUpdateButtonPressed(false);
            }, 1000)
        }
    }, [isUpdateButtonPressed]);


    useEffect(() => {
        async function fetchMyAPI() {
            setToken(await AsyncStorage.getItem('accessToken'))
            callData(await AsyncStorage.getItem('accessToken'), true)
            setInterval(async () => {
                callData(await AsyncStorage.getItem('accessToken'), false)
            }, 60000)
        }
        fetchMyAPI()
    }, [])


    // setTimeout(() => {

    //     callData(token)
    // }, 360000)






    callData = async (token, refresh) => {
        if (refresh) {
            setLoading(true)
        }


        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }


        axios.get('http://apicloud.bsisugarcane.com/api/CYMS/Monitor', {
            headers
        })
            .then((response) => {
                if (refresh) {
                    setLoading(false)
                }

                setData(response.data)
            })
            .catch((error) => {
                if (refresh) {
                    setLoading(false)
                }
                console.log('error response : ', error)
            })
            .finally(function () {
            });

    };

    return (
        <View style={styles.MainContainer}>
            <StatusBar barStyle="light-content" />
            {isLoading ?
                <Loading
                    visible={true}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                :
                <View style={styles.MainContainer}>
                    <ScrollView style={{}} refreshControl={<RefreshControl tintColor='#FFF'
                        onRefresh={() => { callData(token, true) }
                        } />}>
                        <View style={{ marginBottom: 36, alignItems: 'center', marginTop: 12, backgroundColor: '#FFF' }}>

                            <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 22 : 18, fontWeight: 'bold' }}>บริษัท อุตสาหกรรมน้ำตาลบ้านไร่ จำกัด </Text>

                            {/* pharse one */}

                            <View
                                style={{ backgroundColor: '#fffd38', marginTop: 8, height: 36, width: '100%', alignItems: 'center', justifyContent: 'center' }}
                            >


                                <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 16 : 14, fontWeight: 'bold' }}>สรุปยอดประจำฤดูการผลิต รถชั่ง เข้า-ออก(ลานใน)</Text>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginTop: 12,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12 }}>อ้อยสะสม</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6,
                                            color: '#fc0d1b'

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_Wght_All)))}
                                    />



                                </View>
                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 2 }}>ตัน</Text>

                                </View>

                                <View style={{ flex: 0.15 }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถสะสม</Text>
                                </View>

                                <View style={{ flex: 0.3 }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            color: '#fc0d1b',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_Car_All)))}
                                    />

                                </View>

                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 2 }}>เที่ยว</Text>

                                </View>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12 }}>อ้อยสด</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6,
                                            color: '#0b24fb'

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CaneWT1)))}
                                    />



                                </View>
                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 2 }}>ตัน</Text>

                                </View>

                                <View style={{ flex: 0.15 }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>คิดเป็น</Text>
                                </View>

                                <View style={{ flex: 0.3 }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            color: '#0b24fb',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CanePT1)))}
                                    />

                                </View>

                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 2 }}>%</Text>

                                </View>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12 }}>อ้อยไฟไหม้</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6,
                                            color: '#000'

                                        }}
                                        textAlign={'center'}
                                        editable={false} selectTextOnFocus={false}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CaneWT2)))}
                                    />



                                </View>
                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 2 }}>ตัน</Text>

                                </View>

                                <View style={{ flex: 0.15 }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>คิดเป็น</Text>
                                </View>

                                <View style={{ flex: 0.3 }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        editable={false} selectTextOnFocus={false}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CanePT2)))}
                                    />

                                </View>

                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 2 }}>%</Text>

                                </View>

                            </View>


                            {/* phase two */}


                            <View
                                style={{ backgroundColor: '#d9ead4', marginTop: 16, height: 36, width: '100%', alignItems: 'center', justifyContent: 'center' }}
                            >


                                <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 16 : 14, fontWeight: 'bold' }}>สรุปปริมาณการทำงานของรถตัดอ้อย</Text>

                            </View>

                            {/* head phase two */}
                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginTop: 16,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'


                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center' }}>

                                </View>
                                <View style={{ flex: 0.75, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            backgroundColor: '#fff1ce',
                                            height: 30,
                                            fontWeight: 'bold',
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={'จำนวนรถ(คัน)'}
                                    />
                                </View>



                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center' }}>

                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#96040b',
                                            height: 30,
                                            fontWeight: 'bold',
                                            color: '#fff',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={'โรงงาน'}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderTopWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#3a7524',
                                            height: 30,
                                            fontWeight: 'bold',
                                            color: '#fff',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={'ชาวไร่'}
                                    />
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            fontWeight: 'bold',
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={'ยอดสะสม'}
                                    />
                                </View>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: 12 }}>รถตัดอ้อยสด</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Factory_CarCT401)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Farmer_CarCT401)))}
                                    />
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CarC401_007)))}
                                    />
                                </View>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: 12 }}>รถตัดอ้อยไฟไหม้</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format((data.Factory_CarCT402)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Farmer_CarCT402)))}
                                    />
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CarC402_008)))}
                                    />
                                </View>

                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center' }}>
                                    <Text style={{ color: "#0b24fb", fontSize: 12 }}>รวมทั้งหมด</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Factory_CarSC401402)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Farmer_CarSC401402)))}
                                    />
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CarC_All)))}
                                    />
                                </View>

                            </View>


                            {/* head phase two / two */}
                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginTop: 16,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'


                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center' }}>

                                </View>
                                <View style={{ flex: 0.75 }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            backgroundColor: '#fff1ce',
                                            height: 30,
                                            fontWeight: 'bold',
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={'น้ำหนักอ้อย(ตัน)'}
                                    />
                                </View>



                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center' }}>

                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#96040b',
                                            height: 30,
                                            fontWeight: 'bold',
                                            color: '#fff',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={'โรงงาน'}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderTopWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#3a7524',
                                            height: 30,
                                            fontWeight: 'bold',
                                            color: '#fff',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={'ชาวไร่'}
                                    />
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            fontWeight: 'bold',
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={'ยอดสะสม'}
                                    />
                                </View>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: 12 }}>อ้อยสด</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Factory_CarWT401)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Farmer_CarWT401)))}
                                    />
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CarW401_007)))}
                                    />
                                </View>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: 12 }}>อ้อยไฟไหม้</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Factory_CarWT402)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Farmer_CarWT402)))}
                                    />
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CarW402_008)))}
                                    />
                                </View>

                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.25, justifyContent: 'center' }}>
                                    <Text style={{ color: "#0b24fb", fontSize: 12 }}>รวมทั้งหมด</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Factory_CarSW401402)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Farmer_CarSW401402)))}
                                    />
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderBottomWidth: 1,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CarW_All)))}
                                    />
                                </View>

                            </View>


                            {/* phase three */}


                            <View
                                style={{ backgroundColor: '#ff9330', marginTop: 16, height: 36, width: '100%', alignItems: 'center', justifyContent: 'center' }}
                            >


                                <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 16 : 14, fontWeight: 'bold' }}>สรุปยอดประจำวัน รถชั่ง เข้า-ออก(ลานใน)</Text>

                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginTop: 12,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, }}>
                                    <Text style={{ color: "#512D1A", fontSize: 12 }}>รถชั่งทั้งหมด</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_Car_Day)))}
                                    />



                                </View>
                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#512D1A", fontSize: 12, marginLeft: 2 }}>คัน</Text>

                                </View>

                                <View style={{ flex: 0.15 }}>
                                    <Text style={{ color: "#bd5af8", fontSize: 12, marginLeft: 4 }}>อ้อยสด</Text>
                                </View>

                                <View style={{ flex: 0.3 }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CaneWT1_Day)))}
                                    />

                                </View>

                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#bd5af8", fontSize: 12, marginLeft: 2 }}>ตัน</Text>

                                </View>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, }}>
                                    <Text style={{ color: "#512D1A", fontSize: 12 }}>รถที่ชั่งออกแล้ว</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6,
                                            color: '#000'

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_Car_Day_Out)))}
                                    />



                                </View>
                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#512D1A", fontSize: 12, marginLeft: 2 }}>คัน</Text>
                                </View>

                                <View style={{ flex: 0.15 }}>
                                    <Text style={{ color: "#bd5af8", fontSize: 12, marginLeft: 4 }}>อ้อยไฟไหม้</Text>
                                </View>

                                <View style={{ flex: 0.3 }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CaneWT2_Day)))}
                                    />

                                </View>

                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#bd5af8", fontSize: 12, marginLeft: 2 }}>ตัน</Text>

                                </View>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, }}>
                                    <Text style={{ color: "#512D1A", fontSize: 12 }}>รถที่ค้างชั่งออก</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6,
                                            color: '#000'

                                        }}
                                        textAlign={'center'}
                                        editable={false} selectTextOnFocus={false}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_Car_Day_IN)))}
                                    />



                                </View>
                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#512D1A", fontSize: 12, marginLeft: 2 }}>คัน</Text>

                                </View>

                                <View style={{ flex: 0.15 }}>
                                    <Text style={{ color: "#bd5af8", fontSize: 12, marginLeft: 4 }}>รวมทั้งหมด</Text>
                                </View>

                                <View style={{ flex: 0.3 }}>
                                    <TextInput
                                        style={{
                                            fontSize: 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            marginLeft: 4, marginRight: 4,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        editable={false} selectTextOnFocus={false}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Total_CaneWAll_Day)))}
                                    />

                                </View>

                                <View style={{ flex: 0.05 }}>
                                    <Text style={{ color: "#bd5af8", fontSize: 12, marginLeft: 2 }}>ตัน</Text>

                                </View>

                            </View>


                            {/* pharse four */}

                            <View
                                style={{ backgroundColor: '#ffe3a1', marginTop: 16, height: 36, width: '100%', alignItems: 'center', justifyContent: 'center' }}
                            >


                                <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 16 : 14, fontWeight: 'bold' }}>รายงานสรุปประเภทรถ</Text>

                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginTop: 8,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 16 : 14, fontWeight: 'bold', textDecorationLine: 'underline' }}>ประเภทรถ</Text>
                                </View>


                                <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 16 : 14, fontWeight: 'bold', textDecorationLine: 'underline' }}>ลานนอก(คัน)</Text>
                                </View>
                                <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 16 : 14, fontWeight: 'bold', textDecorationLine: 'underline' }}>ลานใน(คัน)</Text>
                                </View>

                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginTop: 8,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถกล่อง</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC5_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderTopWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(parseInt(data.CarTypeC5_IN))}
                                    />
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถกล่อง (ตัวแม่)</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC7_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC7_IN)))}
                                    />
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถกล่อง (ตัวลูก)</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC8_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC8_IN)))}
                                    />
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถไถลากเทเลอร์</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format((data.CarTypeC4_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC4_IN)))}
                                    />
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถหกล้อ</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC6_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC6_IN)))}
                                    />
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถสิบล้อเดี่ยว</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC3_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC3_IN)))}
                                    />
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถพ่วง(ตัวลูก)</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6
                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC2_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC2_IN)))}
                                    />
                                </View>


                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถพ่วง(ตัวแม่)</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC1_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC1_IN)))}
                                    />
                                </View>


                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4 }}>รถอื่นๆ</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC9_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.CarTypeC9_IN)))}
                                    />
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#fc0d1b", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4, fontWeight: 'bold', textDecorationLine: 'underline' }}>รวมรถท้งหมด</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#fc0d1b',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_CarC_IN)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#fc0d1b',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_CarC_OUT)))}
                                    />
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#0b24fb", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4, fontWeight: 'bold', textDecorationLine: 'underline' }}>รวมรถอ้อยสด</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#0b24fb',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_CarCaneT1_IN)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#0b24fb',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_CarCaneT1_OUT)))}
                                    />
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4, fontWeight: 'bold', textDecorationLine: 'underline' }}>รวมรถอ้อยไฟไหม้</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderLeftWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_CarCaneT2_IN)))}
                                    />
                                </View>

                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_CarCaneT2_OUT)))}
                                    />
                                </View>


                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginTop: 18,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.5, justifyContent: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4, fontWeight: 'bold' }}>รวมรถอ้อย (ลานนอก&ลานใน)</Text>
                                </View>
                                <View style={{ flex: 0.3, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            height: 30,
                                            color: '#fc0d1b',
                                            fontWeight: 'bold',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_CarC_IN + data.Sum_CarC_OUT)))}
                                    />
                                </View>

                                <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4, fontWeight: 'bold' }}>คัน</Text>
                                </View>


                            </View>


                            {/* pharse four / one*/}

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginTop: 18,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, fontWeight: 'bold', textDecorationLine: 'underline' }}>รายการ</Text>
                                </View>


                                <View style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, fontWeight: 'bold', textDecorationLine: 'underline' }}>ชั่วโมงปัจจุบัน</Text>
                                </View>
                                <View style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, fontWeight: 'bold', textDecorationLine: 'underline' }}>ย้อนหลัง 1ชม.</Text>
                                </View>

                                <View style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, fontWeight: 'bold', textDecorationLine: 'underline' }}>ย้อนหลัง 2 ชม.</Text>
                                </View>

                                <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#000", fontSize: Platform.OS == 'ios' ? 14 : 12, fontWeight: 'bold', textDecorationLine: 'underline' }}>หน่วย</Text>
                                </View>

                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginTop: 12,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%',


                            }}>
                                <View style={{ flex: 0.15, justifyContent: 'center' }}>
                                    <Text style={{ color: "#244e1c", fontSize: Platform.OS == 'ios' ? 14 : 12, fontWeight: 'bold' }}>อ้อยโรง1</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#ff9330',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R1WA)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderTopWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#ff9330',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R1W1H)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderTopWidth: 1,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#ff9330',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R1W2H)))}
                                    />
                                </View>

                                <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#244e1c", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4, fontWeight: 'bold' }}>ตัน</Text>
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, justifyContent: 'center' }}>
                                    <Text style={{ color: "#244e1c", fontSize: Platform.OS == 'ios' ? 14 : 12, fontWeight: 'bold' }}>อ้อยโรง2</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#ff9330',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format((data.Sum_R2WA)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#ff9330',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R2W1H)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#ff9330',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R2W2H)))}
                                    />
                                </View>

                                <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#244e1c", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4, fontWeight: 'bold' }}>ตัน</Text>
                                </View>


                            </View>

                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, justifyContent: 'center' }}>
                                    <Text style={{ color: "#ff00f8", fontSize: 12, fontWeight: 'bold' }}>รถดั้มโรง1</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R1CA)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(parseInt(data.Sum_R1C1H))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R1C2H)))}
                                    />
                                </View>

                                <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#ff00f8", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4, fontWeight: 'bold' }}>คัน</Text>
                                </View>


                            </View>


                            <View style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: 4,
                                marginRight: 4,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: '2%'

                            }}>
                                <View style={{ flex: 0.15, justifyContent: 'center' }}>
                                    <Text style={{ color: "#ff00f8", fontSize: 12, fontWeight: 'bold' }}>รถดั้มโรง2</Text>
                                </View>
                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderLeftWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R2CA)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R2C1H)))}
                                    />
                                </View>

                                <View style={{ flex: 0.25, }}>
                                    <TextInput
                                        style={{
                                            fontSize: Platform.OS == 'ios' ? 14 : 12,
                                            borderRightWidth: 1,
                                            borderBottomWidth: 1,
                                            borderColor: '#000',
                                            backgroundColor: '#fff',
                                            fontWeight: 'bold',
                                            height: 30,
                                            color: '#000',
                                            paddingBottom: Platform.OS == 'ios' ? 0 : 6

                                        }}
                                        textAlign={'center'}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        editable={false} selectTextOnFocus={false}
                                        value={String(Intl.NumberFormat().format(parseInt(data.Sum_R2C2H)))}
                                    />
                                </View>

                                <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: "#ff00f8", fontSize: Platform.OS == 'ios' ? 14 : 12, marginLeft: 4, fontWeight: 'bold' }}>คัน</Text>
                                </View>


                            </View>




                        </View>

                    </ScrollView>

                </View>
            }
        </View >
    );
}


Main.navigationOptions = ({ navigation }) => ({
    title: 'หนัาหลัก',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff' },
    headerBackTitleStyle: { color: '#fff' },
    headerStyle: { backgroundColor: '#512D1A' },
    headerTintColor: 'black',
    headerRight: () => {
        const { params = {} } = navigation.state;
        return (
            <TouchableOpacity onPress={() => params.setIsUpdateButtonPressed(true)}>
                <Ionicons name="sign-out-alt" solid size={20} color={'#fff'} style={{ marginRight: 12 }} />
            </TouchableOpacity>
        );
    },
})

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        width: 200,
        height: 200,
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    baseText: {
        fontSize: 26,
    },
    baseTextTwo: {
        fontSize: 16,
    },

});

export default Main